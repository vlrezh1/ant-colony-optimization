from random import choices
MAX_RANDOM_VALUE = 1000  # Максимальный вес вершины графа.
K = 10000  # Количество муравьёв за один пробег. Нужно подбирать оптимальные значения для разных размеров матриц.
# Для 5x5 матриц оптимальное значение 30~40, для 10x10 - 2000, 17x17 - 5000+


class Colony:
    ph_matrix = list()  # Вложенные листы со значениями феромона на каждой вершине.
    ALPHA = 1
    BETA = 1
    Q = 1  # Параметр, имеющий значение порядка цены оптимального решения.

    def __init__(self, vertex):
        self.first_vertex = vertex
        self.current_vertex = self.first_vertex
        self.visited_vertices = [self.current_vertex]
        self.path_length = 0

    @staticmethod
    def generate_pheromone_matrix(size):
        Colony.ph_matrix = [[1 for x in range(size)] for x in range(size)]  # Заполнить единицами.

    def make_choice(self, matrix):
        possible_transitions = [vertex for vertex in range(len(matrix)) if vertex not in self.visited_vertices
                                and vertex != self.current_vertex]  # Список возможных переходов из данной вершины.
        nominators = list()
        denominator = 0
        if len(possible_transitions) != 0:
            for vertex in possible_transitions:
                ph_level = Colony.ph_matrix[self.current_vertex][vertex] ** Colony.ALPHA
                heuristic_distance = 1 / matrix[self.current_vertex][vertex] ** Colony.BETA
                nominators.append(ph_level * heuristic_distance)
                denominator += ph_level / heuristic_distance
            chances = [nominator/denominator for nominator in nominators]  # Вероятности переходов в возможные вершины.
            choice = choices(possible_transitions, weights=chances)[0]  # "Рулетка" по вероятностям.
        else:
            choice = self.first_vertex  # Возвращение в первую по счёту вершину.
        self.path_length += matrix[self.current_vertex][choice]
        Colony.ph_matrix[self.current_vertex][choice] += Colony.Q / self.path_length  # Обновление уровня феромона.
        self.current_vertex = choice
        self.visited_vertices.append(choice)

    def run(self, matrix):
        while len(self.visited_vertices) <= len(matrix):
            self.make_choice(matrix)
        return self


def main():
    matrix = list()
    matrix.append(list(map(int, input().split())))
    for i in range(1, len(matrix[0])):
        matrix.append(list(map(int, input().split())))  # Считывание YxY чисел во вложенные листы.
    Colony.generate_pheromone_matrix(len(matrix))

    ants = K
    min_length = len(matrix) * MAX_RANDOM_VALUE
    while ants > 0:
        ant = Colony(ants % len(matrix))  # На первую вершину запускаем по очереди.
        ant.run(matrix)
        if ant.path_length < min_length:
            min_length = ant.path_length
            ants = K  # Если за последние K муравьев нашли минимум, обновляем K.
        ants -= 1
    print(min_length)


if __name__ == '__main__':
    main()
